from django.db import models
from datetime import datetime, date

# Create your models here.


class Schedule(models.Model):
    day = models.CharField('day', max_length = 30)
    date = models.DateTimeField(default = datetime.now)
    time = models.TimeField()
    activity = models.CharField('activities', max_length=30)
    place = models.CharField('place', max_length=30)
    category = models.CharField('category', max_length=30)

    def __str__(self):
        return self.activity

