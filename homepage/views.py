from django.shortcuts import render
from django.shortcuts import redirect
from datetime import datetime, date
from .models import Schedule
from . import forms

def home(request):
    return render(request, 'home2.html')

def profile(request):
    return render(request, 'profile.html')

def portfolio(request):
    return render(request, 'portt.html')

def schedule(request):
	schedules = Schedule.objects.all().values().order_by('date')
	return render(request, 'schedule.html', {'schedules': schedules})

def create_schedule(request):
	if request.method == 'POST':
		form = forms.ScheduleForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('homepage:schedule')
	else:
		form = forms.ScheduleForm()
	
	return render(request, 'create_schedule.html', {'form': form})

def delete_schedule(request, id):
	 if request.method == 'POST':
		 Schedule.objects.filter(id=id).delete()
		 return redirect('homepage:schedule')
	 else:
		 return HttpResponse("/GET not allowed")





