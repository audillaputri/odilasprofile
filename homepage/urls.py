from django.urls import path
from django.contrib import admin
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.create_schedule, name='create_schedule'),
    path('schedule/clear/<int:id>/', views.delete_schedule, name='delete_schedule'),
]